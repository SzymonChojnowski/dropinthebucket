﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WinGame : GameBaseState
{
    public override void OnEnter(GameController controller)
    {
        base.OnEnter(controller);
        controller.winUI.SetActive(true);
        PlayerPrefs.SetInt("levelReached", int.Parse(controller.level) + 1);

        if(int.Parse(CheckSave(controller.level + "/score")) <= controller.score.Amount)
        {
            PlayerPrefs.SetString(controller.level + "/score", controller.score.Amount.ToString());
            PlayerPrefs.SetString(controller.level + "/time", controller.timer.timeText);
        }
    }

    private string CheckSave(string save)
    {
        return PlayerPrefs.GetString(save, "0");
    }

    public override void OnExit(GameController controller)
    {
        base.OnExit(controller);
        controller.winUI.SetActive(false);
        controller.timer.ResetTimer();
        controller.score.Amount = 0;
        EnablePoints(controller.points);
        ReloadLevel(controller);
    }

    public override void HandleInput(GameController controller)
    {
        base.HandleInput(controller);
    }

    public override void OnUpdate(GameController controller)
    {
        base.OnUpdate(controller);
    }
}
