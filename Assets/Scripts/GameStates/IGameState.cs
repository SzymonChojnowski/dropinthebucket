﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IGameState
{
    void OnUpdate(GameController gameController);

    void OnEnter(GameController gameController);

    void OnExit(GameController gameController);

    void ToState(GameController gameController, IGameState targerState);

    void HandleInput(GameController gameController);
}
