﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOver : GameBaseState
{
    public override void OnEnter(GameController controller)
    {
        base.OnEnter(controller);
        controller.player.transform.SetPositionAndRotation(new Vector2(1000, 1000), controller.player.transform.rotation);
        controller.dieSfx.Play();
        controller.musicDelay = controller.gameMusic.time;
        controller.gameMusic.Stop();
        controller.gameOverUI.SetActive(true);
    }

    public override void OnExit(GameController controller)
    {
        controller.gameOverUI.SetActive(false);
        controller.timer.ResetTimer();
        controller.score.Amount = 0;
        EnablePoints(controller.points);
        ResetEnemies(controller.enemies);
        ReloadLevel(controller);
    }

    public override void HandleInput(GameController controller)
    {
        base.HandleInput(controller);
    }

    public override void OnUpdate(GameController controller)
    {
        base.OnUpdate(controller);
    }
}
