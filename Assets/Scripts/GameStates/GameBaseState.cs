﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class GameBaseState : IGameState
{
    static public readonly IGameState STATE_PLAYING = new Playing();
    static public readonly IGameState STATE_DEAD = new GameOver();
    static public readonly IGameState STATE_WIN = new WinGame();

    public virtual void OnEnter(GameController controller) { }
    public virtual void OnExit(GameController controller) { }
    public virtual void ToState(GameController controller, IGameState targetState)
    {
        controller.State.OnExit(controller);
        controller.State = targetState;
        controller.State.OnEnter(controller);
    }

    public virtual void HandleInput(GameController controller)
    {

    }

    public virtual void OnUpdate(GameController controller)
    {

    }

    protected void ReloadLevel(GameController controller)
    {
        controller.player.State.ToState(controller.player, BasePlayerState.STATE_PLAYING);
    }

    protected void EnablePoints(GameObject points)
    {
        for (int i = 0; i < points.transform.childCount; i++)
        {
            points.transform.GetChild(i).gameObject.SetActive(true);
        }
    }

    protected void ResetEnemies(GameObject enemies)
    {
        for (int i = 0; i < enemies.transform.childCount; i++)
        {
            enemies.transform.GetChild(i).gameObject.SetActive(true);
        }
    }
}
