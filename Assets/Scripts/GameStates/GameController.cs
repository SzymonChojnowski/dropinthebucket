﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : Singleton<GameController>
{
    public string level = "0";
    private GameObject waterSpawn;
    public GameObject water;
    public GameObject points;
    public GameObject enemies;
    public Player player;
    public AudioSource gameMusic;
    public AudioSource dieSfx;
    public Timer timer;
    public Counter score;
    public GameObject gameOverUI;
    public GameObject winUI;

    public float musicDelay = 0f;

    public IGameState State { get; set; }

    void Start()
    {
        State = GameBaseState.STATE_PLAYING;
        State.ToState(this, GameBaseState.STATE_PLAYING);
        int playedTimes = PlayerPrefs.GetInt(level + "/played", 0) + 1;
        PlayerPrefs.SetInt(level + "/played", playedTimes);
        var parseLevel = int.Parse(level) + 1;
    }

    void Update()
    {
        State.OnUpdate(this);
    }

    public void ReloadLevel()
    {
        State.ToState(this, GameBaseState.STATE_PLAYING);
    }

    public void ExitGame()
    {
        SceneManager.LoadScene("Menu");
    }

    public void NextLevel()
    {
        var parseLevel = int.Parse(level) + 1;
        if (SceneManager.GetSceneByName("level" + parseLevel.ToString()) != null)
        {
            SceneManager.LoadScene("level" + parseLevel.ToString());
        }
        else
        {
            SceneManager.LoadScene("Menu");
        }
    }

    public void SpawnWater()
    {
        waterSpawn = Instantiate(water, water.transform.position, Quaternion.identity);
    }

    public void DestroyWater()
    {
        Destroy(waterSpawn);
    }

    public void DestroyBullets(GameObject bullets)
    {
        for (int i = 0; i < bullets.transform.childCount; i++)
        {
            Destroy(bullets.transform.GetChild(i).gameObject);
        }
    }

}
