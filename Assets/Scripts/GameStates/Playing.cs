﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Playing : GameBaseState
{
    public override void OnEnter(GameController controller)
    {
        base.OnEnter(controller);
        controller.gameMusic.time = controller.musicDelay;
        controller.gameMusic.Play();
        controller.SpawnWater();
    }

    public override void OnExit(GameController controller)
    {
        base.OnExit(controller);
        controller.DestroyWater();
    }

    public override void HandleInput(GameController controller)
    {
        base.HandleInput(controller);
    }

    public override void OnUpdate(GameController controller)
    {
        base.OnUpdate(controller);
        controller.timer.UpdateTime();
    }
}
