﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MineController : MonoBehaviour {

	public GameObject explosion;

    void OnTriggerEnter2D(Collider2D other)
    {

        if (Util.Instance.IsInLayerMask(other.gameObject.layer, Util.Instance.SHIELD))
        {
            Instantiate(explosion, gameObject.transform.position, gameObject.transform.rotation);
            gameObject.SetActive(false);
        }
    }
}
