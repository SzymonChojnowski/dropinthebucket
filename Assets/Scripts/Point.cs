﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Point : MonoBehaviour
{

    private AudioSource audioSource;
    private SpriteRenderer spriteRenderer;
    private BoxCollider2D collider;

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        collider = GetComponent<BoxCollider2D>();
        spriteRenderer.enabled = true;
    }
    void OnTriggerEnter2D(Collider2D collision)
    {
        if (Util.Instance.IsInLayerMask(collision.gameObject.layer, Util.Instance.PLAYER))
        {
            GameController.Instance.score.Amount += 1;
            audioSource.Play();
            StartCoroutine(Die());
        }
    }

    private IEnumerator Die()
    {
        spriteRenderer.enabled = false;
        collider.enabled = false;
        yield return new WaitForSeconds(1.0f);
        spriteRenderer.enabled = true;
        collider.enabled = true;
        gameObject.SetActive(false);
    }
}
