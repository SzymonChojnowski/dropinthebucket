﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelPicker : MonoBehaviour
{
    [SerializeField] private Level[] levels;
    [SerializeField] private TextMeshProUGUI levelScore;
    [SerializeField] private TextMeshProUGUI levelNumber;
    [SerializeField] private TextMeshProUGUI score;
    [SerializeField] private TextMeshProUGUI time;
    [SerializeField] private TextMeshProUGUI played;
    [SerializeField] private Button playButton;
    [SerializeField] private Image levelPreview;
    private string currentLevel = "0";

    public int levelsCount;

    void Start()
    {
        //ActivateLevels();
        levelsCount = levels.Length;
    }

    public void ActivateLevels()
    {
        int levelReached = PlayerPrefs.GetInt("levelReached", 1);
        for (int i = 0; i < levelsCount; i++)
        {
            if (i + 1 > levelReached)
            {
                levels[i].levelBtn.interactable = false;
                levels[i].GetComponentInChildren<TextMeshProUGUI>().gameObject.SetActive(false);
            }
        }
        ChangeCurrentLevel(levels[0]);
    }

    public void ResetStats()
    {

        for (int i = 0; i < levelsCount; i++)
        {
            PlayerPrefs.SetString(i + 1 + "/score", "0");
            PlayerPrefs.SetString(i + 1 + "/time", "0");
            PlayerPrefs.SetInt(i + 1 + "/played", 0);
            levels[i].score = PlayerPrefs.GetString(levels[i].ToString() + "/score", "0");
            levels[i].time = PlayerPrefs.GetString(levels[i].ToString() + "/time", "0");
            levels[i].played = PlayerPrefs.GetInt(levels[i].ToString() + "/played", 0);

        }
        ChangeCurrentLevel(levels[0]);
    }

    public void ChangeCurrentLevel(Level level)
    {
        currentLevel = level.level;
        levelNumber.SetText(currentLevel);
        score.SetText(level.score);
        time.SetText(level.time);
        played.SetText(level.played.ToString());
        levelScore.SetText(level.levelScore);
        if(!playButton.isActiveAndEnabled)
        {
            playButton.gameObject.SetActive(true);
        }
        playButton.GetComponentInChildren<TextMeshProUGUI>().SetText("PLAY LEVEL " + currentLevel);
        levelPreview.sprite = level.levelImage;
    }

    public void StartGame()
    {
        SceneManager.LoadScene("level" + currentLevel);
    }
}
