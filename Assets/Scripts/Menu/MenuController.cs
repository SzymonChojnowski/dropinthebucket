﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class MenuController : Singleton<MenuController>
{
    [SerializeField] LevelPicker levelPicker;
    [SerializeField] private AudioSource gameMusic;
    private void Start()
    {
        gameMusic.Play();
    }

    public void ResetProgress()
    {
        PlayerPrefs.SetInt("levelReached", 1);

        //  levelPicker.ActivateLevels();
        levelPicker.ResetStats();
    }

    public void ExitGame()
    {
        Application.Quit();
    }
}
