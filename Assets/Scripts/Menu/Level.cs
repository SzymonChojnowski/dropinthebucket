﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Level : MonoBehaviour
{
    public Sprite levelImage;
    [HideInInspector] public Button levelBtn;
    [HideInInspector] public string score;
    [HideInInspector] public string time;
    [HideInInspector] public int played;

    public string level;
    public string levelScore;
    private void Awake()
    {
        levelBtn = GetComponent<Button>();
        levelBtn.onClick.AddListener(OnLevelClick);
    }

    private void Start()
    {
        score = PlayerPrefs.GetString(level.ToString() + "/score", "0");
        time = PlayerPrefs.GetString(level.ToString() + "/time", "0");
        played = PlayerPrefs.GetInt(level.ToString() + "/played", 0);
    }

    private void OnLevelClick()
    {
        GetComponentInParent<LevelPicker>().ChangeCurrentLevel(this);
    }

}
