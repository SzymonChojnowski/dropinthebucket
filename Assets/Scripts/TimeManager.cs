﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeManager : MonoBehaviour
{
    public float TimeControl { get; set; } = 0.05f;

    public void SlowTime()
    {
        Time.timeScale = TimeControl;
        Time.fixedDeltaTime = 0.02f * Time.timeScale;
    }

    public void NormalTime()
    {
        Time.timeScale = 1f;
        Time.fixedDeltaTime = 0.02f;
    }
}
