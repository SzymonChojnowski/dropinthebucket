﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMove : MonoBehaviour
{
    private Vector3 centerCam;
    [SerializeField] private GameObject player;

    private void OnEnable()
    {
        centerCam = new Vector3(0, 5, -10);
        transform.position = centerCam;
    }
    private void Update()
    {
        if (player.transform.position.y <= (double)centerCam.y) return;
        UpdateCenterCam(player.transform.position.y);
    }

    public void UpdateCenterCam(float YPos)
    {
        transform.position = new Vector3(transform.position.x, YPos, transform.position.z);
    }
}
