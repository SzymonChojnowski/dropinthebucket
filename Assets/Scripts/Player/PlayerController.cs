﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : Singleton<PlayerController>
{
    private Camera mainCamera;

    private Vector3 currentMousePosition = Vector3.zero;
    private Vector3 initialMousePosition = Vector3.zero;
    private ShootLine shootLine;

    public Rigidbody2D rb;
    public TimeManager timeManager;

    public bool IsAbleToJump { get; set; } = true;
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        timeManager = GetComponent<TimeManager>();
        shootLine = GetComponent<ShootLine>();
    }

    void OnEnable()
    {
        mainCamera = Camera.main;
    }

    public void MouseDown()
    {
        initialMousePosition = mainCamera.ScreenToWorldPoint(Input.mousePosition) - transform.position;
    }

    public void MouseDrag()
    {
        if (!IsAbleToJump) return;
        timeManager.SlowTime();
        currentMousePosition = mainCamera.ScreenToWorldPoint(Input.mousePosition) - transform.position;
        shootLine.CreateArrow(initialMousePosition - currentMousePosition);
        shootLine.CheckDistance(initialMousePosition, currentMousePosition);
    }

    public void MouseUp()
    {
        if (!IsAbleToJump) return;
        timeManager.NormalTime();
        shootLine.Line.enabled = false;
        shootLine.arrow.SetActive(false);
        shootLine.Jump();
        IsAbleToJump = false;
        rb.velocity = Vector3.zero;
        currentMousePosition = Vector3.zero;
        initialMousePosition = Vector3.zero;
    }

    public void SwitchCamera()
    {
       mainCamera.gameObject.SetActive(
               ((mainCamera.gameObject.activeInHierarchy) ? false : true));
       }
}
