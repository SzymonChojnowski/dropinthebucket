﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BasePlayerState : IPlayerState
{
    public static readonly IPlayerState STATE_DEAD = new PlayerDead();
    public static readonly IPlayerState STATE_PLAYING = new PlayerPlaying();

    public virtual void OnEnter(Player player)
    {

    }

    public virtual void OnExit(Player player)
    {

    }

    public virtual void OnUpdate(Player player)
    {

    }

    public virtual void HandleInput(Player player)
    {

    }

    public virtual void ToState(Player player, IPlayerState targetState)
    {
        player.State.OnExit(player);
        player.State = targetState;
        player.State.OnEnter(player);
    }
}
