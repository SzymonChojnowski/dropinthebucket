﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPlayerState
{

    void OnEnter(Player player);

    void OnExit(Player player);

    void OnUpdate(Player player);

    void HandleInput(Player player);

    void ToState(Player player, IPlayerState targetState);


}
