﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPlaying : BasePlayerState
{

    public override void OnEnter(Player player)
    {
        base.OnEnter(player);
        player.playerController.SwitchCamera();
        player.playerController.IsAbleToJump = false;
        player.playerController.rb.simulated = true;
        player.playerController.rb.velocity = Vector3.zero;
    }

    public override void OnExit(Player player)
    {
        base.OnExit(player);
    }

    public override void OnUpdate(Player player)
    {
        base.OnUpdate(player);
    }

    public override void HandleInput(Player player)
    {
        base.HandleInput(player);
        if (Input.GetMouseButtonDown(0))
        {
            player.playerController.MouseDown();
        }

        if (Input.GetMouseButton(0))
        {
            player.playerController.MouseDrag();
        }

        if (!Input.GetMouseButtonUp(0)) return;
        player.playerController.MouseUp();
    }
}
