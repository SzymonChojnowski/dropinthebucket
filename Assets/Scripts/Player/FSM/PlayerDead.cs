﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDead : BasePlayerState
{

    public override void OnEnter(Player player)
    {
        base.OnEnter(player);
    }

    public override void OnExit(Player player)
    {
        base.OnExit(player);
        player.transform.position = new Vector2(0, 1f);
        player.playerController.timeManager.NormalTime();
        player.playerController.rb.simulated = false;
        player.playerController.SwitchCamera();
    }

    public override void OnUpdate(Player player)
    {
        base.OnUpdate(player);
    }

    public override void HandleInput(Player player)
    {
        base.HandleInput(player);
    }
}
