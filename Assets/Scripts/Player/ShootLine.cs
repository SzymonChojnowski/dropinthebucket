﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootLine : MonoBehaviour
{
    [SerializeField] private float impulseForce = 1.2f;
    [SerializeField] private float maxImpulseRadius = 2.0f;

    private Vector3 currentImpulse = Vector3.zero;
    private Rigidbody2D rb;

    public GameObject arrow;
    public LineRenderer Line { get; private set; }
    public Vector3 shootVector = Vector3.zero;


    void OnEnable()
    {
        rb = GetComponent<Rigidbody2D>();
        Line = GetComponent<LineRenderer>();
        SetLine();
    }

    private void SetLine()
    {
        Line.material = new Material(Shader.Find("Legacy Shaders/Particles/Additive"));
        Line.widthMultiplier = 0.1f;
        Line.positionCount = 2;
        Line.sortingOrder = 5;
    }

    public void CreateArrow(Vector3 mousePosition)
    {
        Line.enabled = true;
        currentImpulse = new Vector3(mousePosition.x, mousePosition.y, 0.0f) * impulseForce;
    }

    public void Jump()
    {
        rb.AddForce(shootVector);
    }

    public void CheckDistance(Vector3 initialMousePosition, Vector3 currentMousePosition)
    {
        if (Vector3.Distance(initialMousePosition, currentMousePosition) <= maxImpulseRadius)
        {
            Line.SetPosition(0, transform.position + (initialMousePosition - currentMousePosition));
            Line.SetPosition(1, transform.position + (initialMousePosition - currentMousePosition) * -1f);
            shootVector = (initialMousePosition - currentMousePosition) * impulseForce;
        }
        else
        {
            Vector3 vector = (initialMousePosition - currentMousePosition).normalized * maxImpulseRadius * impulseForce;
            Line.SetPosition(0, transform.position + vector);
            Line.SetPosition(1, transform.position + vector * -1f);
            shootVector = vector;
        }
        arrow.SetActive(true);
        arrow.transform.position = Line.GetPosition(0);
    }
}
