﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(PlayerController))]
public class Player : MonoBehaviour
{
    private CircleCollider2D coll;
    private GameController gameController;
    public PlayerController playerController;
    public IPlayerState State { get; set; }

    void Awake()
    {
        playerController = GetComponent<PlayerController>();
        coll = GetComponent<CircleCollider2D>();
    }

    void Start()
    {
        gameController = GameController.Instance;
        State = BasePlayerState.STATE_DEAD;
        State.ToState(this, BasePlayerState.STATE_PLAYING);
    }

    void Update()
    {
        State.OnUpdate(this);
        State.HandleInput(this);
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (State == BasePlayerState.STATE_PLAYING)
        {
            if (Util.Instance.IsInLayerMask(collision.gameObject.layer, Util.Instance.PLATFORM))
            {
                playerController.IsAbleToJump = true;
            }
            if (Util.Instance.IsInLayerMask(collision.gameObject.layer, Util.Instance.DEATH))
            {
                
                gameController.State.ToState(gameController, GameBaseState.STATE_DEAD);
                State.ToState(this, BasePlayerState.STATE_DEAD);
            }
            if (Util.Instance.IsInLayerMask(collision.gameObject.layer, Util.Instance.WIN_TRIGGER))
            {
                gameController.State.ToState(gameController, GameBaseState.STATE_WIN);
                collision.gameObject.GetComponent<AudioSource>().Play();
                State.ToState(this, BasePlayerState.STATE_DEAD);
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (Util.Instance.IsInLayerMask(collision.gameObject.layer, Util.Instance.DEATH))
        {
            gameController.State.ToState(gameController, GameBaseState.STATE_DEAD);
            State.ToState(this, BasePlayerState.STATE_DEAD);
        }
    }
}
