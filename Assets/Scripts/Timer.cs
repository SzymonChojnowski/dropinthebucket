﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Timer : MonoBehaviour
{
     private float time;
     public string timeText;

    public void UpdateTime()
    {
        time += Time.fixedUnscaledDeltaTime;
        string seconds = Mathf.Floor(time % 60f).ToString("00");
        string minutes = Mathf.Floor(time / 60f).ToString("00");
        timeText = minutes + ":" + seconds;
        CounterText.SetText(timeText);
    }
    public TextMeshProUGUI CounterText { get; set; }

    void OnEnable()
    {
        CounterText = GetComponent<TextMeshProUGUI>();
    }

    public void ResetTimer()
    {
        time = 0;
    }
}
