﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Util : Singleton<Util>
{
    public LayerMask PLAYER;
    public LayerMask PLATFORM;
    public LayerMask DEATH;
    public LayerMask SHIELD;
    public LayerMask WIN_TRIGGER;

    public bool IsInLayerMask(int layer, LayerMask layermask)
    {
        return layermask == (layermask | (1 << layer));
    }

}
