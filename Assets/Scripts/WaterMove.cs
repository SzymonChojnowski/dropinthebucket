﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterMove : MonoBehaviour
{
    [SerializeField] private float firstDelay = 2.0f;

    [SerializeField] private float riseSpeed = 1.5f;

    private Rigidbody2D rb;
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        StartCoroutine(WaitForRising(firstDelay));
    }

    IEnumerator WaitForRising(float delay)
    {
        yield return new WaitForSeconds(delay);
        Rise();
    }

    private void Rise()
    {
        rb.velocity = new Vector2(0, riseSpeed);
    }
}
