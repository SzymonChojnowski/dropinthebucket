﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{

    private PlayerController thePlayer;
    public GameObject death;

    public float speed = 0.3f;

    private float turnTimer;
    public float timeTrigger;

    private Rigidbody2D myRigidbody;
    private AudioSource audioSource;
    private SpriteRenderer spriteRenderer;
    private CircleCollider2D collider;

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        collider = GetComponent<CircleCollider2D>();
    }

    void Start()
    {
        thePlayer = FindObjectOfType<PlayerController>();
        myRigidbody = GetComponent<Rigidbody2D>();

        turnTimer = 0;
        timeTrigger = 3f;

    }

    void Update()
    {
        myRigidbody.velocity = new Vector3(myRigidbody.transform.localScale.x * speed, myRigidbody.velocity.y, 0f);

        turnTimer += Time.deltaTime;
        if (turnTimer >= timeTrigger)
        {
            TurnAround();
            turnTimer = 0;
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {

        if (Util.Instance.IsInLayerMask(other.gameObject.layer, Util.Instance.SHIELD))
        {
            Instantiate(death, gameObject.transform.position, gameObject.transform.rotation);
            gameObject.SetActive(false);
        }
    }

    void TurnAround()
    {
        if (transform.localScale.x == 1)
        {
            transform.localScale = new Vector3(-1f, 1f, 1f);
        }
        else
        {
            transform.localScale = new Vector3(1f, 1f, 1f);
        }
    }

}
